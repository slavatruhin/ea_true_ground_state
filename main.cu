#include <iostream>
#include <fstream>
#include <vector>

#define n 3 // длина одномерной цепочки,
// бетта, внешнее поле и обменный интеграл полагается = 1.
#define betta 1
#define J 1
#define h 1

struct chain_type
{
    short S[n]{};
    short M=0;
    short E=0;
};

__global__ void ThreadInit(chain_type *chain, unsigned int length)
{
    unsigned int x = threadIdx.x + blockIdx.x*blockDim.x;
    unsigned int bit = x;
    if (x<length)
    {
        for(int i=0; i<n; i++)
        {
            chain[x].S[i] = bit & 1 ? 1 : -1;
            bit >>= 1;
            chain[x].M += chain[x].S[i];
        }
        for(int i=0; i<n; i++)
        {
            chain[x].E += -J*chain[x].S[i]*chain[x].S[(i+1)%n];
        }
        chain[x].E += h*chain[x].M;
        //printf("threadNo %d: %d \n", x, chain[x].M);
    }
}

__global__ void IterationMatrix(int *E, chain_type *chain, unsigned int length)
{
    int x = threadIdx.x + blockIdx.x*blockDim.x;
    int y = threadIdx.y + blockIdx.y*blockDim.y;
    if(x<length && y<length)
    {
        /*printf("threadNo_x %d, _y %d: %d %d %d    %d %d %d \n", thread_No_x, thread_No_y,
               chain[thread_No_x].S[0], chain[thread_No_x].S[1], chain[thread_No_x].S[2],
               chain[thread_No_y].S[0], chain[thread_No_y].S[1], chain[thread_No_y].S[2]);*/
        for(int i=0; i<n; i++)
        {
            E[x+y*length] += -J*chain[x].S[i]*chain[y].S[i];
        }
    }
}

__global__ void StatSumLines(int *E, chain_type *chain, float *Z, unsigned int length)
{
    unsigned int x = threadIdx.x + blockIdx.x*blockDim.x;
    if(x<length)
    {
        //printf("chain0 = %d \n", chain[0].E);
        for(int i=0; i<length; i++)
        {
            Z[x] += expf(-E[i+x*n]*betta-chain[x].E*betta-chain[i].E*betta);
        }
        //printf("Z = %f \n", Z[x]);
    }
}

__global__ void StatSum(float *Z, unsigned int length)
{
    for(int i=1; i<length+1; i++)
    {
        Z[0] += Z[i];
    }
}


int main()
{
    unsigned int length = 2<<(n-1);
    unsigned int dlength = 2<<(2*n-1);
    std::vector<chain_type> chain(length);
    std::vector<int> E(2<<(2*n-1));
    std::vector<float> Z(length);
    int *dev_E;
    float *dev_Z;
    chain_type *dev_chain;
    dim3 blocks(256, 256);
    dim3 threads(32, 32);

    cudaMalloc((void**)&dev_E, dlength*sizeof (int));
    cudaMalloc((void**)&dev_chain, length*sizeof (chain_type));
    cudaMalloc((void**)&dev_Z, n*sizeof (float));
    cudaMemcpy(dev_E, E.data(), dlength*sizeof (int), cudaMemcpyHostToDevice);
    cudaMemcpy(dev_chain, chain.data(), length*sizeof (chain_type), cudaMemcpyHostToDevice);
    ThreadInit<<<65535,1024>>>(dev_chain, length);
    IterationMatrix<<<blocks,threads>>>(dev_E, dev_chain, length);
    StatSumLines<<<65535,1024>>>(dev_E, dev_chain, dev_Z, length);
    StatSum<<<1,1>>>(dev_Z, length);
    cudaMemcpy(E.data(), dev_E, (2<<(2*n-1))*sizeof (int), cudaMemcpyDeviceToHost);
    cudaMemcpy(Z.data(), dev_Z, n*sizeof (float), cudaMemcpyDeviceToHost);
    cudaFree(dev_Z);
    cudaFree(dev_E);
    cudaFree(dev_chain);

    /*for (int i=0; i<length; i++)
    {
        for (int j=0; j<length; j++)
        {
            std::cout << Z[j+i*length] << "B ";
        }
        std::cout << "\n";
    }
    std::ofstream file("matrix.txt");
    for (int i=0; i<length; i++)
    {
        for (int j=0; j<length; j++)
        {
            file << E[j+i*length] << "B ";
        }
        file << "\n";
    }
    file << "Statistical sum = " << Z[0] <<"\n";*/
    std::cout << "matrix side: " << length <<"\n";
    std::cout << "matrix has components: " << dlength <<"\n";
    std::cout << "Statistical sum = " << Z[0] <<"\n";
}